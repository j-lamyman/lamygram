const filters = require('./src/_11ty/filters');
const pluginRss = require("@11ty/eleventy-plugin-rss");
const Image = require("@11ty/eleventy-img");

async function imageShortcode(src, cls, alt, sizes) {
  let metadata = await Image(src, {
    widths: [500],
    formats: ["avif", "jpeg"],
    outputDir: "./_site/images/",
    urlPath: "/images/",
    duration: "1d"
  });

  let imageAttributes = {
    class: cls,
    alt,
    sizes,
    loading: "lazy",
    decoding: "async",
  };

  // You bet we throw an error on missing alt in `imageAttributes` (alt="" works okay)
  return Image.generateHTML(metadata, imageAttributes);
}

module.exports = function (config) {
  
  Object.keys(filters).forEach(filterName => {
    config.addFilter(filterName, filters[filterName])
  })
  
  config.addPlugin(pluginRss);
  
  config.setTemplateFormats([
    "webmanifest",
    "svg",
    "png",
    "xml",
    "njk",
    "html"
  ]);
 
  config.addNunjucksAsyncShortcode("image", imageShortcode);
  
  return {
    markdownTemplateEngine: 'njk',
    dataTemplateEngine: 'njk',
    htmlTemplateEngine: 'njk',
    dir: {
      input: 'src',
      output: '_site'
    }
  };
};