const url = require('url');
var cloudinary = require('cloudinary').v2;


const savePost = async function(existingUrl) {
  const test = 'mht9ktll9yttna6';
  cloudinary.uploader.unsigned_upload(existingUrl, {})
    .then(function (image) {
      console.log();
      console.log("** File Upload (Promise)");
      console.log("* public_id for the uploaded image is generated by Cloudinary's service.");
      console.log("* " + image.public_id);
      console.log(image);
      console.log("* " + image.url);
      return image.url
    })
    .catch(function (err) {
      console.log();
      console.log("** File Upload (Promise)");
      if (err) { console.warn(err); }
    });
}

  exports.handler = async function(event, context) {
    try {
      const myUrl = event.queryStringParameters.url;
    
      const savedResponse = await savePost(myUrl);
  
      // if (savedResponse.statusCode === 200) {
      //   // await rebuildSite();
      //   return { statusCode: 200, body: savedResponse.body }
      // } else {
      //   return savedResponse
      // }
      return { statusCode: 200, body: `URL: ${savedResponse}` };
    } catch (err) {
      return { statusCode: 500, body: `Error: ${err}` };
    }
  
  };