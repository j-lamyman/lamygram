// Code from Bryan Robinson's Smashing Magazine Post: https://www.smashingmagazine.com/2019/10/bookmarking-application-faunadb-netlify-11ty/

var faunadb = require('faunadb'),
    q = faunadb.query;

var adminClient = new faunadb.Client({
  secret: process.env.FAUNA_KEY
});

const savePost = async function(details) {
  const data = {
    data: details
  };
  return adminClient.query(q.Create(q.Collection("posts"), data))
    .then((response) => {
      /* Success! return the response with statusCode 200 */
      console.log("success", response)
      return {
        statusCode: 200,
        body: JSON.stringify(response)
      }
    }).catch((error) => {
      /* Error! return the error with statusCode 400 */
      return  {
        statusCode: 400,
        body: JSON.stringify(error)
      }
    })
}

exports.handler = async function(event, context) {
  try {
    const url = event.queryStringParameters.url;
    const alt = event.queryStringParameters.alt;
    const description = event.queryStringParameters.description;
    
    const savedResponse = await savePost({'url': url, 'alt': alt, 'description': description});

    if (savedResponse.statusCode === 200) {
      // await rebuildSite();
      return { statusCode: 200, body: savedResponse.body }
    } else {
      return savedResponse
    }
    
  } catch (err) {
    return { statusCode: 500, body: `Error: ${err}` };
  }

};