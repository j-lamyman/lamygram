module.exports = {
  formatDate(datePosted) {
    let dayPosted, monthPosted, yearPosted;
    switch (datePosted.getDay()) {
      case 0:
        dayPosted = "Sunday";
        break;
      case 1:
        dayPosted = "Monday";
        break;
      case 2:
        dayPosted = "Tuesday";
        break;
      case 3:
        dayPosted = "Wednesday";
        break;
      case 4:
        dayPosted = "Thursday";
        break;
      case 5:
        dayPosted = "Friday";
        break;
      case 6:
        dayPosted = "Saturday";
    }

    switch(datePosted.getMonth()) {
      case 0:
        monthPosted = "January";
        break;
      case 1:
        monthPosted = "February";
        break;
      case 2:
        monthPosted = "March";
        break;
      case 3:
        monthPosted = "April";
        break;
      case 4:
        monthPosted = "May";
        break;
      case 5:
        monthPosted = "June";
        break;
      case 6:
        monthPosted = "July";
        break;
      case 7:
        monthPosted = "August";
        break;
      case 8:
        monthPosted = "September";
        break;
      case 9:
        monthPosted = "October";
        break;
      case 10:
        monthPosted = "November";
        break;
      case 11:
        monthPosted = "December";
    }

    let dayNumber = datePosted.getDate().toString();
    let lastDigit = dayNumber.charAt(dayNumber.length-1);

    switch(lastDigit) {
      case "1":
        dayPosted+= ` ${dayNumber}st`;
        break;
      case "2":
        dayPosted+= ` ${dayNumber}nd`;
        break;
      case "3":
        dayPosted+= ` ${dayNumber}rd`;
        break;
      default:
        dayPosted+= ` ${dayNumber}th`;
    }

    return `${dayPosted} ${monthPosted} ${datePosted.getFullYear()}`;
  }
}