const Image = require("@11ty/eleventy-img");

(async () => {
  let url = "https://res.cloudinary.com/lamygram/image/upload/v1612662067/mkes3g4y03i9nwelktbw.jpg";
  let stats = await Image(url, {
    widths: [300]
  });

  console.log( stats );
})();