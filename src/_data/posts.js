// Code from Bryan Robinson's Smashing Magazine Post: https://www.smashingmagazine.com/2019/10/bookmarking-application-faunadb-netlify-11ty/

var faunadb = require('faunadb'),
    q = faunadb.query;

var adminClient = new faunadb.Client({
    secret: process.env.FAUNA_KEY
});

function getPosts() {
  return adminClient.query(q.Paginate(
      q.Match(
        q.Index('all_posts')
      )
  ))
  .then((response) => {
      const linkRefs = response.data;
      const getAllLinksDataQuery = linkRefs.map((ref) => {
          return q.Get(ref)
      })
      return adminClient.query(getAllLinksDataQuery).then(ret => {
          return ret
      })
  }).catch(error => {
      return error
  })
}

function mapPosts(data) {
  return data.map(post => {
      const dateTime = new Date(post.ts / 1000);
      return { time: dateTime, ...post.data }
  })
}

module.exports = async function() {
  const data = mapPosts(await getPosts());
  return data.reverse()
}