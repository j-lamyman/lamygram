---
title: "What is this?"
layout: 'layouts/text-post.html'
---

<a href="../.."><span aria-hidden="true">⬅ </span>Back to all posts</a>

## What is Lamygram?
Lamygram is a play on my surname, mixing it with social media sites that end in 'gram'. 

It's a place for me to share my own images without handing over ownership to a large corperation and without having to use their terrible, slow, inaccessible products that try to market everything under the sun to you and sell all of your data.

You can read more about the [IndieWeb](https://indieweb.org/) or watch [Heydon Pickering](https://twitter.com/heydonworks)'s fantastic video: [Why the indie web?](https://briefs.video/videos/why-the-indieweb/), part of the Webbed Brief series.

## How does it work?
Lamygram is built with [Eleventy](https://www.11ty.dev/) a static-site generator which builds everything as static content on this site.

To upload photos to the site, I'm using a Siri Shortcut on my iPhone. The shortcut does the following:
1. Uploads a selected image to the media hosting CDN [Cloudinary](https://cloudinary.com/).
2. Grabs the URL to the image, passes it to a [FaunaDB](https://fauna.com/) database, along with ALT text which describes the image and a caption for the image.
3. Using a [Netlify build hook](https://docs.netlify.com/configure-builds/build-hooks/), Netlify rebuilds the static site and publishes the latest version!

This site was heavily inspired by [Bryan Robinson](https://twitter.com/brob?lang=en)'s article, [Create A Bookmarking Application With FaunaDB, Netlify And 11ty](https://www.smashingmagazine.com/2019/10/bookmarking-application-faunadb-netlify-11ty/) on Smashing Magazine.

## What's next?
I've got a few things that I'd like to add to this project:
* Documenting and releasing the source code
* Adding webmentions to allow for comments
* Adding multiple images per post
* Pagination for the index page
* Adding an RSS feed